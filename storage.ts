import { path } from './deps.ts'

import { getHeaders } from "./request.ts";
import { StorageScope } from "./scopes.ts";
import { Credentials, getCredentials } from "./credentials.ts";


const encoder = new TextEncoder();
const decoder = new TextDecoder();

type BucketName = string;
type ObjectURI = string;
export type StorageURI = `gs://${BucketName}${ObjectURI}`;


export type ObjectResource = {
	kind: "storage#object";
	id: string;
	selfLink: string;
	mediaLink: string;
	name: string;
	bucket: string;
	generation: string;
	metageneration: string;
	contentType: string;
	storageClass: string;
	size: string;
	md5Hash: string;
	crc32c: string;
	etag: string;
	timeCreated: string;
	updated: string;
	timeStorageClassUpdated: string;
}

export type ObjectListResource = {
	kind: 'storage#objects',
	items: ObjectResource[];
}


export class Storage {
	private keyfile: string;
	readonly credentials: Promise<Credentials>;

	constructor(keyfile: string) {
		this.keyfile = keyfile;
		this.credentials = getCredentials(this.keyfile);
	}

	file(objectUri: StorageURI) {
		const uri = new URL(objectUri);
		return new File(this, uri);
	}

	async list(objectUri: StorageURI) {
		const uri = new URL(objectUri);
		const { pathname } = uri;
		const prefix = pathname.startsWith('/') ? pathname.substring(1) : pathname;

		if (uri.protocol !== 'gs:') {
			throw new Error('Invalid protocol. Must use gs:');
		}

		const headers = await getHeaders(this.credentials, [ StorageScope.readOnly ]);
		headers.set('content-type', 'application/json');

		const url = new URL(
			path.join('/storage/v1/b', uri.hostname, 'o'),
			'https://storage.googleapis.com'
		)
		url.searchParams.set('prefix', prefix);

		const resp = await fetch(url, { headers });
		if (!resp.ok) {
			throw new Error(`Failed to list objects. ${resp.status}: ${resp.statusText}`);
		}

		const { items } = await resp.json() as ObjectListResource;
		return items.map(obj => this.file(`gs://${obj.bucket}/${obj.name}`));
	}
}

export class File {
	private storage: Storage;
	private uri: URL;

	get bucket() {
		return this.uri.hostname;
	}

	get name() {
		return path.basename(this.uri.pathname);
	}

	get pathname() {
		if (this.uri.pathname.startsWith('/')) {
			return this.uri.pathname.substring(1);
		} else {
			return this.uri.pathname;
		}
	}

	get encodedPathname() {
		return encodeURIComponent(this.pathname)
	}

	constructor(storage: Storage, uri: URL) {
		this.storage = storage;
		this.uri = uri;

		if (uri.protocol !== 'gs:') {
			throw new Error('Invalid protocol. Must use gs:');
		}
	}

	static equal(file1: File, file2: File): boolean {
		return file1.uri.href === file2.uri.href;
	}


	async exists(): Promise<boolean> {
		const headers = await getHeaders(this.storage.credentials, [ StorageScope.readOnly ]);
		headers.set('content-type', 'application/json');

		const resp = await fetch(
			new URL(
				path.join('/storage/v1/b', this.bucket, 'o', this.encodedPathname),
				'https://storage.googleapis.com'
			),
			{ headers }
		)

		//	dont need this lol
		await resp.body?.cancel();

		if (resp.status === 404) return false;
		if (resp.status === 200) return true;
		throw new Error(`Failed to get file. ${resp.status}: ${resp.statusText}`);
	}

	async get(): Promise<ObjectResource> {
		const headers = await getHeaders(
			this.storage.credentials,
			[ StorageScope.readOnly ]
		);
		headers.set('content-type', 'application/json');

		const resp = await fetch(
			new URL(
				path.join('/storage/v1/b', this.bucket, 'o', this.encodedPathname),
				'https://storage.googleapis.com'
			),
			{ headers }
		)

		if (!resp.ok) {
			throw new Error(`Failed to get object. ${resp.status}: ${resp.statusText}`);
		}

		return resp.json();
	}


	async delete(): Promise<boolean> {
		const headers = await getHeaders(
			this.storage.credentials,
			[ StorageScope.readWrite ]
		)
		headers.set('content-type', 'application/json');

		const url = new URL(
			path.join('/storage/v1/b', this.bucket, 'o', this.encodedPathname),
			'https://storage.googleapis.com'
		);

		const resp = await fetch(url, {
			method: 'delete',
			headers,
		});

		// don't need this lol
		await resp.body?.cancel();

		if (resp.status === 204) return true;
		if (resp.status === 404) return false;
		throw new Error(`Failed to delete object. ${resp.status}: ${resp.statusText}`);
	}


	private async initDownload(writable: WritableStream<Uint8Array>) {
		const headers = await getHeaders(this.storage.credentials, [ StorageScope.readOnly ]);
		headers.set('content-type', 'application/json');

		const url = new URL(
			path.join('/storage/v1/b', this.bucket, 'o', this.encodedPathname),
			"https://storage.googleapis.com"
		)
		url.searchParams.set('alt', 'media');

		const resp = await fetch(url, { headers });
		if (!resp.ok) {
			writable.abort(
				new Error(`Could not download file. ${resp.status}: ${resp.statusText}`)
			)
		}

		resp.body?.pipeTo(writable);
	}

	/**
	 * downloads the contents of the remote object and provides a stream to read from.
	 * @returns {ReadableStream} the stream you can read from
	 */
	download() {
		const { readable, writable } = new TransformStream({
			transform(chunk, ctrl) {
				ctrl.enqueue(decoder.decode(chunk));
			}
		})

		this.initDownload(writable);
		return readable;
	}


	private async initUpload(readable: ReadableStream<Uint8Array>) {
		const headers = await getHeaders(
			this.storage.credentials,
			[ StorageScope.readWrite ]
		);

		const url = new URL(
			path.join('/upload/storage/v1/b', this.bucket, 'o'),
			"https://storage.googleapis.com",
		);
		url.searchParams.set('name', this.pathname);
		url.searchParams.set('uploadType', 'media');

		const resp = await fetch(url, {
			method: 'POST',
			headers,
			body: readable
		});

		if (!resp.ok) {
			await readable.cancel(
				new Error(`Unable to create file. ${resp.status}: ${resp.statusText}`)
			);
		}

		return await resp.json() as ObjectResource;
	}

	/**
	 * Provides an interface to write content for a upload to a GCP object.
	 * Remember, you should await the response object properly to make sure the stream is done.
	 * @returns {{ writable: WritableStream, response: Promise<ObjectResource> }} the writable stream and the response you should await.
	 */
	upload() {
		const { readable, writable } = new TransformStream<any, Uint8Array>({
			transform(chunk, ctrl) {
				if (chunk instanceof Uint8Array) ctrl.enqueue(chunk);
				else ctrl.enqueue(encoder.encode(chunk));
			}
		});

		const response = this.initUpload(readable);
		return { writable, response };
	}
}
