import { AssertionError, assertEquals } from "https://deno.land/std@0.216.0/assert/mod.ts";

import { Storage } from "../storage.ts";
import { path, fs } from "../deps.ts";
const storage = new Storage('./secrets/gcp.json');

// forcing the creds to finish reading before starting tests
await storage.credentials;


const files = {
	notfound: storage.file('gs://journeywise_test_bucket/tests/404.txt'),
	found: storage.file('gs://journeywise_test_bucket/tests/get.txt'),
	download: storage.file('gs://journeywise_test_bucket/tests/download.txt'),
	upload: storage.file('gs://journeywise_test_bucket/tests/upload.txt'),
	processed: storage.file('gs://journeywise_test_bucket/tests/processed.txt'),

	csv: storage.file('gs://journeywise_test_bucket/tests/test.csv'),
	zip: storage.file('gs://journeywise_test_bucket/tests/text.zip'),
}


const collect = async (readable: ReadableStream) => {
	let content = '';
	for await (const cont of await readable) {
		content += cont;
	}
	return content;
}


Deno.test({ name: 'list objects', async fn() {
	const items = await storage.list('gs://journeywise_test_bucket/tests');
	const remoteFiles = items.map(file => file.pathname);

	const expectedFiles = [
		'tests/',
		'tests/download.txt',
		'tests/get.txt',
	];

	expectedFiles.forEach(expectedFile => {
		if (!remoteFiles.includes(expectedFile)) {
			throw new AssertionError(`Expected file was not found: ${expectedFile}`)
		}
	})
}})


Deno.test({ name: 'file exists', async fn() {
	assertEquals(await files.notfound.exists(), false);
	assertEquals(await files.found.exists(), true);
}})

Deno.test({ name: 'get file', async fn() {
	const {
		kind,
		selfLink,
		mediaLink,
		name,
		bucket,
		contentType
	} = await files.found.get();

	const mediaUrl = new URL(mediaLink);
	assertEquals(mediaUrl.host, "storage.googleapis.com");
	assertEquals(mediaUrl.pathname, "/download/storage/v1/b/journeywise_test_bucket/o/tests%2Fget.txt");
	assertEquals(mediaUrl.searchParams.get('alt'), 'media');

	assertEquals(
		{ kind, selfLink, name, bucket, contentType },
		{
			kind: 'storage#object',
			selfLink: 'https://www.googleapis.com/storage/v1/b/journeywise_test_bucket/o/tests%2Fget.txt',
			name: 'tests/get.txt',
			bucket: 'journeywise_test_bucket',
			contentType: 'text/plain',
		}
	);
}})


Deno.test({ name: 'download stream', async fn() {
	const resp = await collect(files.download.download());
	assertEquals(resp, "This is a download test file.");
}})

Deno.test({ name: 'upload stream', async fn() {
	const { writable, response } = files.upload.upload();
	const writer = writable.getWriter();

	const content = 'This is an upload test file. ' + Date.now();
	await writer.write(content);
	await writer.close();
	await response;

	const confirm = await collect(files.upload.download());
	assertEquals(content, confirm);
}})


Deno.test({ name: 'process file - reupload', async fn() {
	const { writable, response } = files.processed.upload();

	files.upload.download()
		.pipeThrough(new TransformStream({
			flush(ctrl) {
				ctrl.enqueue('\nProcessed file complete');
			}
		}))
		.pipeTo(writable);

	await response
}})


Deno.test({ name: 'delete file', async fn() {
	const worked = await files.processed.delete();
	const exists = await files.processed.exists();

	assertEquals(worked, true);
	assertEquals(exists, false);
}})


Deno.test({ name: 'upload a csv', ignore: true, async fn() {
	const { writable, response } = files.csv.upload();

	const file = await Deno.open(
		'./test/organizations-100.csv',
		{ read: true }
	)

	file.readable.pipeTo(writable);
	await response;
}})

Deno.test({ name: 'upload a zip', ignore: true, async fn() {
	const { writable, response } = files.zip.upload();

	const file = await Deno.open(
		'./test/organizations-100.zip',
		{ read: true }
	)

	file.readable.pipeTo(writable);
	await response;
}})