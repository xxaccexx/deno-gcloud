export type Scope = (
	| CloudScope
	| StorageScope
)

export enum CloudScope {
	fullControl = "https://www.googleapis.com/auth/cloud-platform",
	readOnly = "https://www.googleapis.com/auth/cloud-platform.read-only"
}

export enum StorageScope {
	fullControl = "https://www.googleapis.com/auth/devstorage.full_control",
	readOnly = "https://www.googleapis.com/auth/devstorage.read_only",
	readWrite = "https://www.googleapis.com/auth/devstorage.read_write",
}
