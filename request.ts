import { Scope } from "./scopes.ts";
import { Credentials, getAccessKey } from "./credentials.ts";

export const getHeaders = async (creds: Promise<Credentials>, scopes: Scope[]) => {
	const headers = new Map<string, string>();

	const token = await getAccessKey(creds, scopes);
	headers.set('Authorization', `Bearer ${token}`);

	return headers;
}