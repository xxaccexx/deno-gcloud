import { join } from "https://deno.land/std@0.200.0/path/join.ts";
import { generateJWT } from "./jwt.ts";

export type Credentials = {
	project_id?: string;
	client_email: string;
	private_key: string;
};

type TokenResponse = {
	access_token: string;
	expires_in: number;
	token_type: 'Bearer' | string;
}

const store = {
	tokens: new Map<string, string>(),
	scopes: new Map<string, string[]>(),
	expiry: new Map<string, number>(),
	modify: new Set<string>(),
} as const;

const TOKEN_TTL = 3600 * 24;

const isValid = (client: string): boolean => {
	if (!store.tokens.get(client)) return false;
	if (store.modify.has(client)) return false;

	const now = Date.now() / 1000;
	const expiry = store.expiry.get(client)!;

	return now - expiry < TOKEN_TTL;
}

export const getCredentials = async (keyfile: string) => {
	const keyFilename = join(Deno.cwd(), keyfile);

	try {
		const file = await Deno.open(keyFilename, { read: true, write: false });
		const stream = file.readable.pipeThrough(new TextDecoderStream());

		let content = '';
		for await (const chunk of stream) content += chunk;

		return JSON.parse(content) as Credentials;
	}

	catch {
		throw new Error(`Unable to load keyfile: '${keyFilename}'`);
	}
}


export const appendScope = (client: string, scopes: string[]): string[] => {
	const oScopes = store.scopes.get(client) ?? scopes;

	const nScopes = [...new Set([...oScopes, ...scopes])];

	store.scopes.set(client, nScopes);
	store.modify.add(client);
	return store.scopes.get(client) ?? []
}

export const removeScope = (client: string, scopes: string[]): string[] => {
	const nScopes = store.scopes.get(client) ?? scopes
	store.scopes.set(
		client,
		nScopes.filter(scope => !scopes.includes(scope)),
	)
	store.modify.add(client)
	return store.scopes.get(client) ?? []
}

export const getAccessKey = async (credentials: Promise<Credentials>, scopes: string[] = []) => {
	const { client_email, private_key } = await credentials;

	if (isValid(client_email)) return store.tokens.get(client_email);
	const assertion = await generateJWT(
		{ client_email, private_key },
		appendScope(client_email, scopes),
	);

	const resp = await fetch('https://oauth2.googleapis.com/token', {
		method: 'POST',
		headers: { 'content-type': 'application/x-www-form-urlencoded' },
		body: new URLSearchParams({
			grant_type: 'urn:ietf:params:oauth:grant-type:jwt-bearer',
			assertion,
		})
	});

	if (!resp.ok) {
		console.log(Deno.inspect(await resp.json(), {
			colors: true,
			depth: Infinity,
			strAbbreviateSize: Infinity,
		}))
		throw new Error('API Error')
	}

	const data = await resp.json() as TokenResponse;

	store.tokens.set(client_email, data.access_token);
	store.expiry.set(client_email, data.expires_in);
	store.modify.delete(client_email);

	return data.access_token;
}
