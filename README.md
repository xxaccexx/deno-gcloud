# Google Cloud APIs

This library was originally to add a deno interface for GCP cloud APIs, but has been scoped back to just exists/get/upload/download of single objects in GCS.

There might be plans to add other services or interfaces later, but im a lazy boi.


## Google Cloud Storage

To get a reference to the GCS object

```ts
import { Storage } from 'https://gitlab.com/xxaccexx/deno-gcloud/-/raw/v1.0.0/mod.ts'

const storage = new Storage('./relative/path/to/tkeyfile.json');
const file = storage.file('gs://bucket_name/file_name.ext');
```

Alternatively, construct a file reference directly.

```ts
import { File } from 'https://gitlab.com/xxaccexx/deno-gcloud/-/raw/v1.0.0/mod.ts'
const file = new File('./relative/path/to/tkeyfile.json', 'gs://bucket_name/file_name.ext');
```


# APIs

## StorageURI

This is a string that is expected to start with the `gs:` prototcol, followed by the bucket name, then path to object.
```ts
type BucketName = string;
type ObjectURI = string;
type StorageURI = `gs://${BucketName}${ObjectURI}`;
```


## Storage interface

**new Storage(keyfilename: string)**<br/>
Instatiate a new Storage instance. Requires the path to the keyfile containing your access. Currently only supports service account with no support for user based auth, unless you download a keyfile for your user. Also only supports json format, not p12 or other formats.

**.file(objectUri: StorageURI): File**<br/>
Instatiate a new File instance. Requires the uri for the object. Though this object does not need to exist. Will handle passing the keyfile ref along.

**.list(objectUri: StorageURI): Promise<File[]>**<br/>
Returns a promise for a list of File instances. Each being an object at the path.<br/>
`objectUri` can be the root of a bucket or any directory within one. eg: `gs://my_bucket/` or `gs://my_bucket/some/directory/`

## File interface

**new File(storage: Storage, objectUri: StorageURI)**<br/>
Instatiate a new File instance. Requires both a parent Storage instance and the uri of the object.

**.exists(): Promise<boolean>**<br/>
Will resolve to true if the response status is 200. If the status is 404, will resolve to false. Any other status codes will throw an error.<br/>
[GCS status and error codes docs](https://cloud.google.com/storage/docs/json_api/v1/status-codes)

**.get(): Promise<ObjectResource>**<br/>
If response is 200, will resolve to the resource object [from GCS docs](https://cloud.google.com/storage/docs/json_api/v1/objects#resource). Anything other that 200 will throw an error.<br/>
[GCS status and error codes docs](https://cloud.google.com/storage/docs/json_api/v1/status-codes)

**.upload(): WritableStream<any>**<br/>
Always returns a writable stream. Through it might close early if there are problems.<br/>
You can write to this stream and the data will overwrite any existing data in the remote file, if it exists.<br/>
If it does not exist then the object will be created. Other [GCS status and error codes](https://cloud.google.com/storage/docs/json_api/v1/status-codes) will close the stream with an error.

**.download(): { writable: ReadableStream<string>, response: Promise<ObjectResource> }**<br/>
Response is the resource object [from GCS docs](https://cloud.google.com/storage/docs/json_api/v1/objects#resource)<br/>

Always returns an object containing the writable stream and will close early if there are problems.<br/>
You can read the stream which contains the data from the file on GCS, if it exists.<br/>
If it does not exist or any other [GCS status and error codes](https://cloud.google.com/storage/docs/json_api/v1/status-codes), the stream will close with an error.


# Limitations

Currently, you are not able to stream a file from GCS back to the same file on GCS, since there is no attept to cache written data while downloading it. Basically, reading the file from GCS locks the file so the write action does not take effect.

Steps around this, you could download the file then upload it again, later.

# Future plans

This is not the be all and end all. Though IDK when I'll have time to get to it.
But here's a couple things I wanna do...

## File

**.append(): WriteStream<an>**<br/>
to stream content onto the end of a file, good for CSVs etc.<br/>

**.rename(): Promise<boolean>**<br/>
Take a file and rename it, good for temp files which could support the limitation above.
