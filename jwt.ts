import * as base64url from 'https://deno.land/std@0.200.0/encoding/base64url.ts'
import * as base64 from 'https://deno.land/std@0.200.0/encoding/base64.ts';
import { Credentials } from "./credentials.ts";

const buf = new TextEncoder();

export const TOKEN_TTL = 3600;

const getNow = () => Math.floor(Date.now() / 1000);



type JwtHeader = {
	alg: 'RS256' | string;
	typ: 'JWT' | string;
}

type JwtPayload = {
	iat: number;
	exp: number;
	aud: "https://oauth2.googleapis.com/token" | string;
	iss: string;
	scope: string;
}


export const generateSigningKey = (private_key: string) => {
	const keyHeader = "-----BEGIN PRIVATE KEY-----\n";
	const keyFooter = "-----END PRIVATE KEY-----\n";

	const keyContent = private_key
		.replace(keyHeader, '')
		.replace(keyFooter, '')
		.replaceAll('\n', '');

	const keyData = base64.decode(keyContent);

	return crypto.subtle.importKey(
		'pkcs8',
		keyData,
		{ name: 'RSASSA-PKCS1-v1_5', hash: 'SHA-256' },
		false,
		['sign']
	)
}

export const generateJWT = async (creds: Credentials, scopes: string[] = [], iat = getNow()): Promise<string> => {
	const { client_email, private_key } = creds;

	const header: JwtHeader = {
		alg: 'RS256',
		typ: 'JWT'
	}

	const payload: JwtPayload = {
		iat, exp: iat + TOKEN_TTL,
		aud: 'https://oauth2.googleapis.com/token',
		iss: client_email,
		scope: scopes.join(' '),
	}

	const key = await generateSigningKey(private_key);

	const signingInput = [
		base64url.encode(buf.encode(JSON.stringify(header))),
		base64url.encode(buf.encode(JSON.stringify(payload)))
	].join('.');

	const signature = base64url.encode(await crypto.subtle.sign(
		{ name: "RSASSA-PKCS1-v1_5", hash: { name: "SHA-256" } },
		key, buf.encode(signingInput)
	))

	const token = [
		signingInput,
		signature
	].join('.');

	return token;
}